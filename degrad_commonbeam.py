import sys
import numpy as np
from astropy.io import fits as fits
from scipy.stats import multivariate_normal
from astropy.convolution import Gaussian2DKernel
from astropy.convolution import convolve

def Sigma(bmaj, bmin, theta):
    ## Creates covariance matrix [2x2] that defines 2D Gaussian function
    ## from header parametes. theta in degrees! 

    theta*=np.pi/180. ## to rad
    sx = (bmaj/(np.sqrt(8.*np.log(2.))))
    sy = (bmin/(np.sqrt(8.*np.log(2.))))
    
    sxx=sx**2
    syy=sy**2
    
    a = np.cos(theta)**2/(2.*sxx) + np.sin(theta)**2/(2.*syy)
    b = -np.sin(2.*theta)/(4.*sxx) + np.sin(2.*theta)/(4.*syy)
    c = np.sin(theta)**2/(2.*sxx) + np.cos(theta)**2/(2.*syy)
    
    sigma_1 =  np.array([[a, -b], [-b, c]])
    sigma = np.linalg.inv(sigma_1)
    
    return sigma

def beam(bmaj, bmin, theta, nx, ny):
    ## theta in degrees!
    ## create a 2D gaussian from header parameters. Requieres nx, ny,
    ## size of the array.
    ## Uses Sigma function defined above.
    
    # Our 2-dimensional distribution will be over variables X and Y
    X = np.linspace(-int(nx/2.), int(nx/2.), nx)
    Y = np.linspace(-int(ny/2.), int(ny/2.), ny)
    X, Y = np.meshgrid(X, Y)
    
    # Pack X and Y into a single 3-dimensional array
    pos = np.empty(X.shape + (2,))
    pos[:, :, 0] = X
    pos[:, :, 1] = Y
    
    # Mean vector and covariance matrix
    mu = np.array([0., 0.]) # center position in pixels
    sigma=Sigma(bmaj,bmin,theta)
    
    F = multivariate_normal(mu, sigma)
    Z = F.pdf(pos)
    
    return Z


def beamS(Sigma,nx,ny):
    ## creates 2D Gaussian from covariance matrix. 

    #  2D distribution will be over variables X and Y
    X = np.linspace(-int(nx/2.), int(nx/2.), nx)
    Y = np.linspace(-int(ny/2.), int(ny/2.), ny)
    X, Y = np.meshgrid(X, Y)
    
    # Pack X and Y into a single 3-dimensional array
    pos = np.empty(X.shape + (2,))
    pos[:, :, 0] = X
    pos[:, :, 1] = Y

    # Mean vector
    mu = np.array([0., 0.])

    # uses scipy.stats multivariate normal module
    F = multivariate_normal(mu, Sigma)
    Z = F.pdf(pos)
    
    return Z

def conv(fin,fref,fout,savebeams=False):
    ## smoothes image "fin" to angular resolution of fref
    # e.g. conv('map_39.fits' ,'map_17.fits', 'map_39_as+17.fits')
    
    im1 = fits.open(fin)[0].data
    hin = fits.open(fin)[0].header
    
    imref = fits.open(fref)[0].data
    href = fits.open(fref)[0].header
    
    ## read beam parameters from image and ref image
    bmaj1 = hin['BMAJ']
    bmin1 = hin['BMIN']
    bpa1  = hin['BPA']
    cdelt2= hin['CDELT2']
    
    ## read beam parameters from 
    bmaj2 = href['BMAJ']
    bmin2 = href['BMIN']
    bpa2  = href['BPA']
        
    ## create covariance matrix for beam 1 in pixel units
    Sigma1=Sigma(bmaj1/cdelt2,bmin1/cdelt2,bpa1+90.)
    
    ## create covariance matrix for beam ref in pixel units
    Sigma2=Sigma(bmaj2/cdelt2,bmin2/cdelt2,bpa2+90.)
    
    # covariance of the convolution kernel (just the difference
    # between the reference and the original covariances)
    Sigma_conv = Sigma2-Sigma1    
    
    ## define kernel size based on ref beam size
    ## we use a size of ~10 sigma
    sigma_ref = (bmaj2 + bmin2)/2. / np.sqrt(8.*np.log(2.)) / cdelt2
    nx_k = np.rint(10.*sigma_ref).astype(int)

    # kernel size must be odd 
    if nx_k % 2 == 0:
        # even
        nx_k+=1
    else:
        pass # Odd

    
    ny_k = nx_k # square kernel
    kernel=beamS(Sigma_conv,nx_k,ny_k)

    import re

    ## scale factor to correct intensity values in case units are Jy/beam
    unitscale=1.0
    if (re.search("BEAM",hin['BUNIT']),re.I):
        print("Jy/beam units")
        beam_in=(np.pi/(4.*np.log(2.)))*bmaj1*bmin1
        beam_out=(np.pi/(4.*np.log(2.)))*bmaj2*bmin2
        unitscale=beam_out/beam_in
        
    ## performs the convolution
    smooth = convolve(im1, kernel)
    smooth *= unitscale
    
    CheckCenter=True
    if CheckCenter:
        spike=np.zeros(im1.shape)
        (ny,nx)=spike.shape
        print("spike dims",nx,ny)
        spike[int(ny/2.),int(nx/2.)]=1.
        smooth_spike = convolve(spike, kernel)
        
    ## update beam parameters in header
    hin['BMAJ']=bmaj2
    hin['BMIN']=bmin2
    hin['BPA']=bpa2

    # export smoothed file
    fits.writeto(fout,smooth, hin, overwrite=True)
    
    if savebeams:
        ## save beams as fits files for analysis
        beam1=beamS(Sigma1,nx_k,ny_k)
        beam2=beamS(Sigma2,nx_k,ny_k)
        conv = convolve(beam1, kernel)
        
        fits.writeto('beam1.fits',beam1, hin,overwrite=True)
        fits.writeto('beam2.fits',beam2, hin,overwrite=True)
        fits.writeto('b1_conv_b2.fits',conv, hin,overwrite=True)
        fits.writeto('kernel.fits',kernel,hin, overwrite=True)
        fits.writeto('diff.fits',(beam2-conv)/conv,hin, overwrite=True)
        
    if CheckCenter:
        fits.writeto('spike.fits',spike,hin, overwrite=True)
        fits.writeto('smooth_spike.fits',smooth_spike,hin, overwrite=True)
    
        
    return

